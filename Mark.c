#include <stdio.h>

//Compiler version gcc  6.3.0

int main()
{
  int marks[5][3],i,j,max_marks;
  for(i=0;i<5;i++)
  { 
    printf("enter the marks obtained by students %d",i);
    for(j=0;j<3;j++)
    {
      printf("\nmarks[%d][%d]=",i,j);
      scanf("%d",marks[i][j]);
    }
    for(j=0;j<3;j++)
    {
      max_marks=-999;
      for(i=0;i<5;i++)
      {
        if(max_marks<marks[i])
        {
          max_marks=marks[i][j];
        }
      }
      printf("the highest marks obtained in the subject %d=%d",i,max_marks);
    }
  
  }
  return 0;
}